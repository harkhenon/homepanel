# Homepanel, the simpliest web panel ever

## What is homepanel?

Homepanel is a little and simple web panel for managing websites hosting and domains management.
Created with Laravel and React especially on a Raspberry PI 4 (Ubuntu Server 21.04).

It is designed for home webhosting (little website or portfolios).

## Why on a raspberry PI?

Buying a Raspberry PI 4 Model B 8 Go is more easy than a server, or a monthly paid VPS.
For 80/90$-€, you can literraly have an Ubuntu server in your home and do WHAT YOU WANT with, not only install Homepanel.
You can use it like a PC, a domotic hub and many other uses.

## Documentation

Sorry, no documentation available for the moment, give me a little piece of time for developping this tool ;)
